// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Project_MGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECT_M_API AProject_MGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
